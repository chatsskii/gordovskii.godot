﻿using Godot;

namespace Gordovskii.Godot.Scenes
{
    /// <summary>
    /// Class for switching between scenes.
    /// </summary>
    public class SceneManager<RootNode> : ISceneManager where RootNode : Node
    {
        protected readonly RootNode _rootNode;

        public SceneManager(RootNode rootNode)
        {
            _rootNode = rootNode ?? throw new ArgumentNullException(nameof(rootNode));
        }

        public void ToScene<TScene>(ISceneScript<TScene> sceneScript, TScene scene) where TScene : class
        {
            if (sceneScript == null)
                throw new ArgumentNullException(nameof(sceneScript));

            if (scene == null)
                throw new ArgumentNullException(nameof(scene));

            if (sceneScript is Node sceneNode)
            {
                sceneScript.Initialize(scene);

                foreach (var child in _rootNode.GetChildren())
                    child.QueueFree();

                _rootNode.AddChild(sceneNode);
            }
            else
                throw new ArgumentException(nameof(sceneScript));
        }
        public void ToScene<TScene>(PackedScene packedScene, TScene scene) where TScene : class
        {
            if (packedScene == null)
                throw new ArgumentNullException(nameof(packedScene));

            if (scene == null)
                throw new ArgumentNullException(nameof(scene));

            var unpackedScene = packedScene.Instantiate();
            if (unpackedScene is ISceneScript<TScene> sceneScript)
                ToScene(sceneScript, scene);
            else
                throw new ArgumentException(nameof(packedScene));
        }
    }
}
