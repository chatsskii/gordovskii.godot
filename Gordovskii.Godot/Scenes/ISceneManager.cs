﻿using Godot;

namespace Gordovskii.Godot.Scenes
{
    /// <summary>
    /// Interface for switching between scenes.
    /// </summary>
    public interface ISceneManager
    {
        void ToScene<TSceneModel>(ISceneScript<TSceneModel> sceneScript, TSceneModel scene) where TSceneModel : class;
        void ToScene<TSceneModel>(PackedScene packedScene, TSceneModel scene) where TSceneModel : class;
    }
}
