﻿using Godot;

namespace Gordovskii.Godot.Scenes
{
    /// <summary>
    /// Abstract scene script class.
    /// </summary>
    /// <typeparam name="TScene">Scene model type.</typeparam>
    public abstract partial class SceneScript<TScene> : Node, ISceneScript<TScene> where TScene : class
    {
        /// <summary>
        /// Injected scene model.
        /// </summary>
        private TScene? _scene;
        public bool IsInitialized => _scene != null;

        /// <summary>
        /// Returns injecting scene model.
        /// </summary>
        public TScene? Scene => _scene;

        public void Initialize(TScene scene)
        {
            if (IsInitialized)
                throw new InvalidOperationException(nameof(Initialize));

            _scene = scene ?? throw new ArgumentNullException(nameof(scene));
            DoInitialize(_scene);
        }

        /// <summary>
        /// <inheritdoc cref="Initialize(TScene)"/>
        /// </summary>
        /// <param name="model"><inheritdoc cref="Initialize(TScene)"/></param>
        protected abstract void DoInitialize(TScene model);
    }
}
