﻿namespace Gordovskii.Godot.Scenes
{
    /// <summary>
    /// Interface for scene script.
    /// </summary>
    /// <typeparam name="TScene"></typeparam>
    public interface ISceneScript<TScene> where TScene : class
    {
        /// <summary>
        /// Returns <see langword="true"/> if script is already initialized.
        /// </summary>
        public bool IsInitialized { get; }

        /// <summary>
        /// Initialize scene script by injecting <paramref name="scene"/>.
        /// </summary>
        /// <param name="scene">Scene model for injection.</param>
        public void Initialize(TScene scene);
    }
}
