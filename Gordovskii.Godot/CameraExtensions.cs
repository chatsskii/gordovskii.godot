﻿using Godot;
using Gordovskii.Godot.Raycast;

namespace Gordovskii.Godot
{
    public static class CameraExtensions
    {
        public static bool InsideView(this Camera3D camera, Vector3 position)
            => RaycastHelper.Raycast(camera.GlobalPosition, position, camera.GetWorld3D().DirectSpaceState) == null;
    }
}
